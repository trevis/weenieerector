﻿using Decal.Adapter.Wrappers;
using Decal.Interop.Inject;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace WeenieErector.Lib {
    public class MarkerManager : IDisposable {
        private const int DRAW_INTERVAL_MS = 100;

        private DateTime lastThought = DateTime.UtcNow;
        private bool isLoggedIn = false;
        private uint drawingLandblock = 0;

        private List<D3DObj> drawnObjects = new List<D3DObj>();
        private Dictionary<int, float> cachedHeights = new Dictionary<int, float>();

        private bool disposed = false;

        public MarkerManager() {
            Globals.Core.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            Globals.LocationManager.LandblockChanged += LocationManager_LandblockChanged;
            Globals.SpawnManager.SpawnEdited += SpawnManager_SpawnEdited;
            Globals.SpawnManager.SpawnCreated += SpawnManager_SpawnCreated;
        }

        private void SpawnManager_SpawnCreated(object sender, SpawnCreatedEventArgs e) {
            try {
                RedrawMarkers();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void SpawnManager_SpawnEdited(object sender, SpawnEditedEventArgs e) {
            try {
                RedrawMarkers();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void LocationManager_LandblockChanged(object sender, LandblockChangedEventArgs e) {
            try {
                if (isLoggedIn && e.Landblock != 0) {
                    drawingLandblock = e.Landblock;
                    RedrawMarkers();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                isLoggedIn = true;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Think() {
            if (isLoggedIn && DateTime.UtcNow - lastThought > TimeSpan.FromMilliseconds(DRAW_INTERVAL_MS)) {
                lastThought = DateTime.UtcNow;

                var markerCount = Globals.SpawnManager.GetWeeniesFromLandblock(drawingLandblock).Count;

                if (drawingLandblock != 0 && drawnObjects.Count != markerCount) {
                    RedrawMarkers();
                }
            }
        }

        private void RedrawMarkers() {
            var markerCount = Globals.SpawnManager.GetWeeniesFromLandblock(drawingLandblock).Count;
            var landblock = Util.CurrentLandblock();
            int lbxoffset = (int)(Math.Floor((double)(landblock) / (double)(0x1000000))) & 0xFF;
            int lbyoffset = (int)(Math.Floor((double)(landblock) / (double)(0x10000))) & 0xFF;

            // remove old
            ClearMarkers();

            // draw new
            foreach (var weenie in Globals.SpawnManager.GetWeeniesFromLandblock(drawingLandblock)) {
                var origin = weenie.pos.frame.origin;
                var x = ((double)(lbxoffset - 0x7F) * 192 + origin.x - 84) / 240d;
                var y = ((double)(lbyoffset - 0x7F) * 192 + origin.y - 84) / 240d;
                var text = string.IsNullOrEmpty(weenie.desc) ? "No Description" : weenie.desc;

                text = string.Format("{0} - {1} ({2})", weenie.id, text, weenie.wcid);

                var offset = 2.75F;
                int objid = Globals.SpawnManager.FindWeenieWorldObjectId(weenie);

                if (cachedHeights.ContainsKey(weenie.wcid)) {
                    offset = cachedHeights[weenie.wcid];
                }
                else if (objid != 0) {
                    try {
                        offset = Globals.Core.Actions.Underlying.ObjectHeight(objid) + 0.2F;
                        cachedHeights.Add(weenie.wcid, offset);
                    }
                    catch { offset = 2.75F; }
                }

                var desc = Globals.Core.D3DService.MarkCoordsWith3DText((float)y, (float)x, (float)origin.z + offset, text, "Arial", (int)eFontOptions.eFontBold);
                desc.Scale(0.175F);
                desc.OrientToCamera(true);

                if (Globals.SpawnManager.addedWeenies.Contains(weenie.GetUniqueKey())) {
                    desc.Color = Color.LightGreen.ToArgb();
                }
                else if (Globals.SpawnManager.modifiedWeenies.Contains(weenie.GetUniqueKey())) {
                    desc.Color = Color.LightPink.ToArgb();
                }

                drawnObjects.Add(desc);
            }
        }

        private void ClearMarkers() {
            foreach (var obj in drawnObjects) {
                try {
                    obj.Visible = false;
                    obj.Dispose();
                }
                catch { }
            }

            drawnObjects.Clear();
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    ClearMarkers();
                    Globals.SpawnManager.SpawnEdited -= SpawnManager_SpawnEdited;
                    Globals.Core.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                    Globals.LocationManager.LandblockChanged -= LocationManager_LandblockChanged;
                }
                disposed = true;
            }
        }
    }
}
