﻿using System;
using System.IO;
using VirindiViewService;
using VirindiViewService.Controls;
using WeenieErector.Data.WorldSpawns;
using WeenieErector.Lib;

namespace WeenieErector.Views {
    public class Nudge {
        HudStaticText UINudgeTarget { get; set; }
        HudTextBox UINudgeXYAmount { get; set; }
        HudTextBox UINudgeZAmount { get; set; }
        HudButton UINudgeXPos { get; set; }
        HudButton UINudgeXNeg { get; set; }
        HudButton UINudgeYPos { get; set; }
        HudButton UINudgeYNeg { get; set; }
        HudButton UINudgeZPos { get; set; }
        HudButton UINudgeZNeg { get; set; }

        Weenie current { get; set; }

        public Nudge(HudView view) {
            UINudgeTarget = (HudStaticText)view["NudgeTarget"];
            UINudgeXYAmount = (HudTextBox)view["NudgeXYAmount"];
            UINudgeZAmount = (HudTextBox)view["NudgeZAmount"];
            UINudgeXPos = (HudButton)view["NudgeXPos"];
            UINudgeXNeg = (HudButton)view["NudgeXNeg"];
            UINudgeYPos = (HudButton)view["NudgeYPos"];
            UINudgeYNeg = (HudButton)view["NudgeYNeg"];
            UINudgeZPos = (HudButton)view["NudgeZPos"];
            UINudgeZNeg = (HudButton)view["NudgeZNeg"];

            UINudgeXPos.Hit += (s, e) => { NudgeX(1); };
            UINudgeXNeg.Hit += (s, e) => { NudgeX(-1); };
            UINudgeYPos.Hit += (s, e) => { NudgeY(1); };
            UINudgeYNeg.Hit += (s, e) => { NudgeY(-1); };
            UINudgeZPos.Hit += (s, e) => { NudgeZ(1); };
            UINudgeZNeg.Hit += (s, e) => { NudgeZ(-1); };

            Globals.SpawnManager.WcidSpawned += SpawnManager_WcidSpawned;
        }

        private void NudgeX(int v) {
            double amount = 0.1;
            Double.TryParse(UINudgeXYAmount.Text, out amount);
            DecalProxy.DispatchChatToBoxWithPluginIntercept("/nudge_x " + (amount * v));
            if (current != null) Globals.SpawnManager.NudgeWeenie(current, (amount * v), 0, 0);
        }

        private void NudgeY(int v) {
            double amount = 0.1;
            Double.TryParse(UINudgeXYAmount.Text, out amount);
            DecalProxy.DispatchChatToBoxWithPluginIntercept("/nudge_y " + (amount * v));
            if (current != null) Globals.SpawnManager.NudgeWeenie(current, 0, (amount * v), 0);
        }

        private void NudgeZ(int v) {
            double amount = 0.1;
            Double.TryParse(UINudgeZAmount.Text, out amount);
            DecalProxy.DispatchChatToBoxWithPluginIntercept("/nudge_z " + (amount * v));
            if (current != null) Globals.SpawnManager.NudgeWeenie(current, 0, 0, (amount * v));
        }

        private void SpawnManager_WcidSpawned(object sender, WcidSpawnedEventArgs e) {
            try {
                UINudgeTarget.Text = string.Format("{0} - {1}", e.Weenie.id, e.Weenie.desc);
                current = e.Weenie;
            }
            catch (Exception ex) {
                Util.LogException(ex);
            }
        }
    }
}
