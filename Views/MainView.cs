﻿using System;
using System.Drawing;
using System.IO;
using VirindiViewService;
using VirindiViewService.XMLParsers;
using WeenieErector.Lib;

namespace WeenieErector.Views {
    public class MainView : IDisposable {
        public readonly VirindiViewService.HudView view;

        private ViewProperties properties;
        private ControlGroup controls;

        public SpawnWeenie SpawnWeenieView;
        public EditSpawns EditSpawnsView;
        public Nudge NudgeView;
        public ImportExport ImportExportView;

        private bool disposed;

        public MainView() {
            try {
                new Decal3XMLParser().ParseFromResource("WeenieErector.Views.mainView.xml", out properties, out controls);

                view = new VirindiViewService.HudView(properties, controls);

                SpawnWeenieView = new SpawnWeenie(view);
                EditSpawnsView = new EditSpawns(view);
                NudgeView = new Nudge(view);
                ImportExportView = new ImportExport(view);

                view.Icon = GetIcon();

                //view.Visible = true;
                view.ShowInBar = true;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public ACImage GetIcon() {
            ACImage acImage = null;

            try {
                using (Stream manifestResourceStream = typeof(MainView).Assembly.GetManifestResourceStream("WeenieErector.Resources.Icons.eggplant.png")) {
                    if (manifestResourceStream != null) {
                        using (Bitmap bitmap = new Bitmap(manifestResourceStream))
                            acImage = new ACImage(bitmap);
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return acImage;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    if (SpawnWeenieView != null) SpawnWeenieView.Dispose();
                    if (EditSpawnsView != null) EditSpawnsView.Dispose();
                    if (view != null) view.Dispose();
                }
                disposed = true;
            }
        }
    }
}
