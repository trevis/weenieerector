﻿namespace WeenieErector.Data.WorldSpawns {
    public class WeenieFrame {
        public WeenieFrameAngles angles = new WeenieFrameAngles();
        public WeenieFrameOrigin origin = new WeenieFrameOrigin();

        public WeenieFrame() {
        }
    }
}