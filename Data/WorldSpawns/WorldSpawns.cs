﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeenieErector.Data.WorldSpawns {
    public class WorldSpawns {
        public string _version;
        public List<Landblock> landblocks = new List<Landblock>();

        public void SetVersion() {
            _version = string.Format("GDLE Worldspawns File Version 1.41 - Total objects mapped {0}", GetTotalMappedWeenies());
        }

        public int GetTotalMappedWeenies() {
            int total = 0;

            foreach (var landblock in landblocks) {
                total += landblock.value.weenies.Count;
            }

            return total;
        }
    }
}
