﻿using System;

namespace WeenieErector.Data.WorldSpawns {
    public class Landblock {
        public long key = 0;

        public LandblockValue value = new LandblockValue();

        internal void ReindexWeenies() {
            uint index = 1;

            foreach (var weenie in value.weenies) {
                if (weenie.id != index) {
                    foreach (var link in value.links) {
                        if (link.source == weenie.id) link.source = index;
                        if (link.target == weenie.id) link.target = index;
                    }
                    weenie.id = index;
                }
                index++;
            }
        }
    }
}