# Weenie Erector
> Allows editing of worldspawns.js weenies/links ingame. Uses the following admin commands to update spawns: /spawnwcid, /removethis, /nudge_x, /nudge_y, /nudge_z. (these commands are not required to be supported on the server, it's just helpful visually.)

![](https://i.gyazo.com/2a2001e6c4a5ec7d5e708205713802dd.mp4)

## Installation

Download Installer: [WeenieErectorInstaller-1.0.0.exe](/uploads/39c98810a96501994425ddb572927061/WeenieErectorInstaller-1.0.0.exe)

## Features
* Import / export worldspawns.json
* Spawn / Edit / Delete weenies
* Add / Remove links

## How to Use
* Install the plugin
* Optional: add weenies.csv to `\Documents\Decal Plugins\WeenieErector` (can download lifestoned content google doc csv ingame)
* Add your worldspawns.json to `\Documents\Decal Plugins\WeenieErector\worldspawns`
* Load game, import worldspawns.json from the Import/Export tab
* Spawn / Edit weenies
* Export worldspawns.json on the Import/Export tab
* Copy worldspawns.json back over to your server

## Known Issues
* If you edit a landblock that is defined in cache and not worldspawns, it will overwrite the cache with your new changes.  If you want to keep cache data you will need to manually merge it into worldspawns before importing with weenie erector.

## Release History

* 0.0.1
    * Initial

## Additional screenshots
![](https://i.gyazo.com/2a2001e6c4a5ec7d5e708205713802dd.mp4)
![](https://i.gyazo.com/4cd9a43ed4d15d5a9231aa19651f0134.mp4)
![](https://i.gyazo.com/0627331e5822b275fb8341b2b478b184.mp4)
![](https://i.gyazo.com/7eceb8787e57d60eae30b4237945426a.png)

